/*! \file  initializeSerialPort.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 14, 2015, 7:56 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "../include/graphicsHostLibrary.h"

void clearSerial( void );

/*! initializeSerialPort - */

/*!
 *
 */
void initializeSerialPort(void)
{
  serialInitialize(0);
  clearSerial();
}
