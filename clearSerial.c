/*! \file  clearSerial.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 14, 2015, 7:46 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

/*! clearSerial - */

/*!
 *
 */
void clearSerial(void)
{
  unsigned char waste;
  waste = U1RXREG;
  waste = U1RXREG;
  waste = U1RXREG;
  waste = U1RXREG;
  waste = U1RXREG;
  _OERR = 0;

}

