/*! \file  touchTest1.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 10, 2015, 10:06 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../include/graphicsHostLibrary.h"
#include "../include/colors.h"

/* Configuration fuses */
#pragma config FNOSC = FRCPLL       // Fast RC oscillator with PLL
#pragma config FWDTEN = OFF         // Turn off watchdog timer

void initializeSerialPort( void );

#define LINE
/*! main - */

/*!
 *
 */
int main(void)
{
  int i;
  unsigned int x, y;
#ifdef LINE
  unsigned int oldx, oldy;
#endif

  // LED will be used for general signaling
  _TRISB9 = 0;

  // LED flashing lets me know when the application actually
  // starts sending data.  LED is on a second, flashed quickly
  // 5 times, stays on a second, goes off a second, then data starts
  _LATB9 = 1;

  serialInitialize(0);

  delay(1000);
  for (i = 0; i < 5; i++)
    {
      _LATB9 = 0;
      delay(100);
      _LATB9 = 1;
      delay(100);
    }
  delay(1000);
  _LATB9 = 0;
  delay(1000);

  // Sync just to be sure, then initialize TFT and Touch
  idleSync();
  TFTinit(TFTLANDSCAPE);
  TFTclear();
  TFTsetColorX(HOTPINK);
  TFTsetFont(FONTDJS);
  putString(50, 50, 8, "Landscape");
  delay(1000);

  //putch(TFTTOUCHINIT);
  //putch(1);

  // Dull, boring colors
  TFTsetColorX(WHITE);
  TFTsetBackColorX(BLACK);

      TFTsetFont(FONTDJS);

  while (1)
    {
      TFTclear();
      delay(1000);
#ifdef LINE
      oldx = oldy = 9000;
#endif 
      putString(100, 5, 8, "Running");
      for (i = 0; i < 10000; i++)
        {
          if (TFTgetTouch(&x, &y))
#ifdef LINE
            {
              if (oldx < 1000)
                TFTline(oldx, oldy, x, y);
              oldx = x;
              oldy = y;

            }
          else
            oldx = oldy = 9000;
#else
              TFTpixel(x,y);
#endif 
        }
    }
  return 0;
}
