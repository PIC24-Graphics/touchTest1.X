## Trace touch on screen with line

Simple test of touch, permits the user to scribble on the screen with
a stylus or finger.  Redraws the screen every few minutes to erase.
